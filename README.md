简介
====
Qtc_CppCheck是一个Qt Creator IDE的插件，通过调用CppCheck程序检查代码文件，将检查结果直观地显示在IDE中，实现随时检查、快速定位和修改Bug的功能，目前只检测C++。（需要安装CppCheck程序，http://cppcheck.net/）

使用说明
========
下载
--------
下载bin\8.0.1目录下的qtc_cppcheck.dll文件，这是一个已经编译好的插件动态库文件，适用于官网下载的Qt Creator （windows）8.0.1及其以后的版本程序，将qtc_cppcheck.dll文件复制到 C:\Qt\Tools\QtCreator\lib\qtcreator\plugins 目录下，重新启动Qt Creator，即会自动加载该插件，在QtCreator菜单"Help"-"About Plugins..."中能够看到该插件
![](http://git.oschina.net/zhouliqiu/qtc_cppcheck/raw/master/screenshot/about.png)

选项
--------
在QtCreator菜单"Tools"-"Options..."中可以看到该插件的设置项，选择正确的CppCheck程序的路径。
![](http://git.oschina.net/zhouliqiu/qtc_cppcheck/raw/master/screenshot/option.png)

使用
--------
### 检查当前文件
打开一个源文件，使用快捷键"Alt+F9"，或者选择菜单"Tools"-"CppCheck检查"-"检查当前文件"
### 检查当前项目
选择菜单"Tools"-"CppCheck检查"-"检查当前项目"，弹出检查当前项目对话框，从下拉框中选择项目，勾选需要检查的文件，点击“开始检测”，注意：检测当前项目时，请保存所有当前打开的文档，否则当前修改不会被检查到。
![](http://git.oschina.net/zhouliqiu/qtc_cppcheck/raw/master/screenshot/project.png)
### 检查项目管理器
在项目管理器中，选择一个节点，点击右键，选择"使用CppCheck检查"

![](http://git.oschina.net/zhouliqiu/qtc_cppcheck/raw/master/screenshot/right-click.png)
### 检查结果
检查结果显示IDE的问题列表中
![](http://git.oschina.net/zhouliqiu/qtc_cppcheck/raw/master/screenshot/check-result.png)
