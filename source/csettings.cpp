/****************************************************************************
** Copyright (C) 2017 ZhouLiQiu(zhouliqiu@126.com).
**
** This file is part of the qtc_cppcheck, It is a plugin for Qt Creator.
**
** Permission is hereby granted, free of charge, to any person obtaining a copy
** of this software and associated documentation files (the "Software"), to deal
** in the Software without restriction, including without limitation the rights
** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
** copies of the Software, and to permit persons to whom the Software is
** furnished to do so, subject to the following conditions:
**
** The above copyright notice and this permission notice shall be included in
** all copies or substantial portions of the Software.
**
** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
** THE SOFTWARE.
**
****************************************************************************/
#include "csettings.h"
#include "qtc_cppcheckconstants.h"
#include <coreplugin/icore.h>
#include <QSettings>
#include <QThread>

namespace qtc_cppcheck {
namespace Internal {

CSettings::CSettings(QObject *parent) : QObject(parent)
{
    readSettings();
}

QStringList CSettings::getCheckArgString()
{
    QStringList argu;
    argu << QLatin1String("--language=c++");
    argu << QLatin1String("--template={file},{line},{severity},{id},{message}");

    if (getPopupWarn())
    {
        QString enableStr = QLatin1String("--enable=warning");
        enableStr.append(QLatin1String(",performance"));
        enableStr.append(QLatin1String(",portability"));
        enableStr.append(QLatin1String(",style"));
        argu << enableStr;
    }
    int coreNum = QThread::idealThreadCount();
    if (coreNum > 1)
    {
        argu << QString("-j %1").arg(coreNum);
    }
    return argu;
}

void CSettings::saveSettings()
{
    QSettings *settings = Core::ICore::settings();
    settings->beginGroup(QLatin1String(qtc_cppcheck::Constants::SETTING_GROUP));
    settings->setValue(QLatin1String(qtc_cppcheck::Constants::SETTING_EXE_FILENAME), m_exeFileName);
    settings->setValue(QLatin1String(qtc_cppcheck::Constants::SETTING_POPUP_ERROR), m_popupError);
    settings->setValue(QLatin1String(qtc_cppcheck::Constants::SETTING_POPUP_WARN), m_popupWarn);
    settings->endGroup();
}

QString CSettings::getExeFileName() const
{
    return m_exeFileName;
}

void CSettings::setExeFileName(const QString &fileName)
{
    m_exeFileName = fileName;
}

bool CSettings::getPopupError() const
{
    return m_popupError;
}

void CSettings::setPopupError(bool b)
{
    m_popupError = b;
}

bool CSettings::getPopupWarn() const
{
    return m_popupWarn;
}

void CSettings::setPopupWarn(bool b)
{
    m_popupWarn = b;
}

void CSettings::readSettings()
{
    QSettings *settings = Core::ICore::settings();
    settings->beginGroup(QLatin1String(qtc_cppcheck::Constants::SETTING_GROUP));
    m_exeFileName = settings->value(QLatin1String(qtc_cppcheck::Constants::SETTING_EXE_FILENAME)).toString();
    m_popupError = settings->value(QLatin1String(qtc_cppcheck::Constants::SETTING_POPUP_ERROR), true).toBool();
    m_popupWarn = settings->value(QLatin1String(qtc_cppcheck::Constants::SETTING_POPUP_WARN), true).toBool();
    settings->endGroup();
}

} // namespace Internal
} // namespace qtc_cppcheck
