/****************************************************************************
** Copyright (C) 2017 ZhouLiQiu(zhouliqiu@126.com).
**
** This file is part of the qtc_cppcheck, It is a plugin for Qt Creator.
**
** Permission is hereby granted, free of charge, to any person obtaining a copy
** of this software and associated documentation files (the "Software"), to deal
** in the Software without restriction, including without limitation the rights
** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
** copies of the Software, and to permit persons to whom the Software is
** furnished to do so, subject to the following conditions:
**
** The above copyright notice and this permission notice shall be included in
** all copies or substantial portions of the Software.
**
** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
** THE SOFTWARE.
**
****************************************************************************/
#include "optionpage.h"
#include "qtc_cppcheckconstants.h"
#include <coreplugin/icore.h>
#include "csettings.h"
#include "optionwidget.h"

namespace qtc_cppcheck {
namespace Internal {

OptionPage::OptionPage(QObject *parent) :
    Core::IOptionsPage(parent)
{
    setId(qtc_cppcheck::Constants::OPTIONS_ID);
    setCategory("CppCheck");
    setDisplayCategory(QString("CppCheck检查"));
    setDisplayName(QString("选项"));
    setCategoryIcon(Utils::Icon(":/res/cppcheck-gui.png"));
    m_settings = new CSettings(this);  // auto delete
}

QWidget *OptionPage::widget()
{
    if (!m_widget)
    {
        m_widget = new OptionWidget(m_settings, NULL);
    }
    return m_widget;
}

void OptionPage::apply()
{
    m_widget->saveSettings();
}

void OptionPage::finish()
{

}

} // namespace Internal
} // namespace qtc_cppcheck
