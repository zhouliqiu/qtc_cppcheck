/****************************************************************************
** Copyright (C) 2017 ZhouLiQiu(zhouliqiu@126.com).
**
** This file is part of the qtc_cppcheck, It is a plugin for Qt Creator.
**
** Permission is hereby granted, free of charge, to any person obtaining a copy
** of this software and associated documentation files (the "Software"), to deal
** in the Software without restriction, including without limitation the rights
** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
** copies of the Software, and to permit persons to whom the Software is
** furnished to do so, subject to the following conditions:
**
** The above copyright notice and this permission notice shall be included in
** all copies or substantial portions of the Software.
**
** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
** THE SOFTWARE.
**
****************************************************************************/
#ifndef WORKER_CHECK_H
#define WORKER_CHECK_H

#include <QObject>
#include <QStringList>
#include <QProcess>
#include <QTemporaryFile>

namespace ProjectExplorer {
    class Node;
}

namespace qtc_cppcheck {
namespace Internal {

class Worker_Check : public QObject
{
    Q_OBJECT
public:
    explicit Worker_Check(QObject *parent = 0);
    ~Worker_Check();

    bool isRunning();
    void checkFiles(QStringList &listFile);
    void checkNode(const ProjectExplorer::Node* node);
    void stopCheck();

signals:
    void sig_check_progress(int value);
    void sig_finished(int exitCode, QProcess::ExitStatus exitStatus,
                      int error, int warn, int style, int portability, int performance);

private slots:
    void slot_readOutput();
    void slot_readErrors();
    void slot_error(QProcess::ProcessError error);
    void slot_finished(int exitCode, QProcess::ExitStatus exitStatus);
    void slot_started();

private:
    QProcess *m_process;
    QTemporaryFile *m_tempFile;

    int m_error_num;
    int m_warn_num;
    int m_portability_num;
    int m_style_num;
    int m_performance_num;

    QStringList m_supportFileType;
    bool isCheckable(QString fileName);  // 检查一个文件的后缀名，看看是否可以被检测
    QStringList getNodeFiles(const ProjectExplorer::Node* node);
};

} // namespace Internal
} // namespace qtc_cppcheck

#endif // WORKER_CHECK_H
