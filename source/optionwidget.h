/****************************************************************************
** Copyright (C) 2017 ZhouLiQiu(zhouliqiu@126.com).
**
** This file is part of the qtc_cppcheck, It is a plugin for Qt Creator.
**
** Permission is hereby granted, free of charge, to any person obtaining a copy
** of this software and associated documentation files (the "Software"), to deal
** in the Software without restriction, including without limitation the rights
** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
** copies of the Software, and to permit persons to whom the Software is
** furnished to do so, subject to the following conditions:
**
** The above copyright notice and this permission notice shall be included in
** all copies or substantial portions of the Software.
**
** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
** THE SOFTWARE.
**
****************************************************************************/
#ifndef OPTION_WIDGET_H
#define OPTION_WIDGET_H

#include <QWidget>

namespace qtc_cppcheck {
namespace Internal {

namespace Ui {
class OptionWidget;
}

class CSettings;

class OptionWidget : public QWidget
{
    Q_OBJECT

public:
    explicit OptionWidget(CSettings *settings, QWidget *parent = 0);
    ~OptionWidget();

    void saveSettings();

private slots:
    void onbtnSelectclicked();

private:
    Ui::OptionWidget *ui;
    CSettings *m_settings;
    void setControls();
};

} // namespace Internal
} // namespace qtc_cppcheck

#endif // OPTION_WIDGET_H
