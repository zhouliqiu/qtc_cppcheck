/****************************************************************************
** Copyright (C) 2017 ZhouLiQiu(zhouliqiu@126.com).
**
** This file is part of the qtc_cppcheck, It is a plugin for Qt Creator.
**
** Permission is hereby granted, free of charge, to any person obtaining a copy
** of this software and associated documentation files (the "Software"), to deal
** in the Software without restriction, including without limitation the rights
** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
** copies of the Software, and to permit persons to whom the Software is
** furnished to do so, subject to the following conditions:
**
** The above copyright notice and this permission notice shall be included in
** all copies or substantial portions of the Software.
**
** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
** THE SOFTWARE.
**
****************************************************************************/
#ifndef CSETTINGS_H
#define CSETTINGS_H

#include <QString>
#include <QObject>

namespace qtc_cppcheck {
namespace Internal {

class CSettings : public QObject
{
    Q_OBJECT
public:
    explicit CSettings(QObject *parent = NULL);

    QStringList getCheckArgString();  // 获取选项参数

    void saveSettings();

    QString getExeFileName() const;
    void setExeFileName(const QString &fileName);

    bool getPopupError() const;
    void setPopupError(bool b);

    bool getPopupWarn() const;
    void setPopupWarn(bool b);

private:
    QString m_exeFileName;
    bool m_popupError;
    bool m_popupWarn;

    void readSettings();
};

} // namespace Internal
} // namespace qtc_cppcheck

#endif // CSETTINGS_H
