#ifndef QTC_CPPCHECKPLUGIN_H
#define QTC_CPPCHECKPLUGIN_H

#if defined(QTC_CPPCHECK_LIBRARY)
#  define QTC_CPPCHECK_EXPORT Q_DECL_EXPORT
#else
#  define QTC_CPPCHECK_EXPORT Q_DECL_IMPORT
#endif

#include <extensionsystem/iplugin.h>

namespace qtc_cppcheck {
namespace Internal {

class OptionPage;
class Worker_Check;
class qtc_cppcheckPlugin : public ExtensionSystem::IPlugin
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "org.qt-project.Qt.QtCreatorPlugin" FILE "qtc_cppcheck.json")

public:
    qtc_cppcheckPlugin();
    ~qtc_cppcheckPlugin() override;

    bool initialize(const QStringList &arguments, QString *errorString) override;
    void extensionsInitialized() override;
    ShutdownFlag aboutToShutdown() override;

private:
    OptionPage *m_optionPage;
    Worker_Check *m_worker_check;
    void triggerActionNode();
    void triggerActionFile();
    void triggerActionProject();
};

} // namespace Internal
} // namespace qtc_cppcheck

#endif // QTC_CPPCHECKPLUGIN_H
