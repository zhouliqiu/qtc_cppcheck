/****************************************************************************
** Copyright (C) 2017 ZhouLiQiu(zhouliqiu@126.com).
**
** This file is part of the qtc_cppcheck, It is a plugin for Qt Creator.
**
** Permission is hereby granted, free of charge, to any person obtaining a copy
** of this software and associated documentation files (the "Software"), to deal
** in the Software without restriction, including without limitation the rights
** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
** copies of the Software, and to permit persons to whom the Software is
** furnished to do so, subject to the following conditions:
**
** The above copyright notice and this permission notice shall be included in
** all copies or substantial portions of the Software.
**
** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
** THE SOFTWARE.
**
****************************************************************************/
#include "optionwidget.h"
#include "ui_optionwidget.h"
#include "csettings.h"
#include <QFileDialog>
#include <QPushButton>
#include <QDebug>

namespace qtc_cppcheck {
namespace Internal {

OptionWidget::OptionWidget(CSettings *settings, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::OptionWidget),
    m_settings(settings)
{
    ui->setupUi(this);
    setControls();
    connect(ui->btnSelect, SIGNAL(clicked(bool)), this, SLOT(onbtnSelectclicked()));
}

OptionWidget::~OptionWidget()
{
    delete ui;
}

void OptionWidget::saveSettings()
{
    Q_ASSERT(m_settings != NULL);
    m_settings->setExeFileName(ui->editExeFile->text());
    m_settings->setPopupError(ui->chkPopupError->isChecked());
    m_settings->setPopupWarn(ui->chkPopupWarn->isChecked());
    m_settings->saveSettings();
}

void OptionWidget::setControls()
{
    Q_ASSERT(m_settings != NULL);
    ui->editExeFile->setText(m_settings->getExeFileName());
    ui->chkPopupError->setChecked(m_settings->getPopupError());
    ui->chkPopupWarn->setChecked(m_settings->getPopupWarn());
}

void OptionWidget::onbtnSelectclicked()
{
    QString fileName = QFileDialog::getOpenFileName(this, QString("请选择CppCheck的执行程序"));
    if (!fileName.isEmpty())
    {
        ui->editExeFile->setText(QDir::toNativeSeparators(fileName));
    }
}

} // namespace Internal
} // namespace qtc_cppcheck
