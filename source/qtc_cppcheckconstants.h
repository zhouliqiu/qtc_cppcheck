#ifndef QTC_CPPCHECKCONSTANTS_H
#define QTC_CPPCHECKCONSTANTS_H

namespace qtc_cppcheck {
namespace Constants {

const char GLOBAL_ID[] = "Qtc.CppCheck";

const char OPTIONS_ID[] = "Qtc.CppCheck.Option";

const char ACTION_NODE_ID[] = "Qtc.CppCheck.Action.Node";
const char ACTION_FILE_ID[] = "Qtc.CppCheck.Action.File";
const char ACTION_PROJECT_ID[] = "Qtc.CppCheck.Action.Project";
const char MENU_ID[] = "Qtc.CppCheck.Menu";

const char SETTING_GROUP[] = "Qtc.CppCheck.Setting.Group";
const char SETTING_EXE_FILENAME[] = "Qtc.CppCheck.Setting.ExeFileName";
const char SETTING_POPUP_ERROR[] = "Qtc.CppCheck.Setting.PopupError";
const char SETTING_POPUP_WARN[] = "Qtc.CppCheck.Setting.PopupWarn";

const char TASK_CATEGORY_ID[] = "Qtc.CppCheck.TaskCategory";
const char TASK_CATEGORY_NAME[] = "CppCheck检查结果";

} // namespace Constants
} // namespace qtc_cppcheck

#endif // QTC_CPPCHECKCONSTANTS_H
