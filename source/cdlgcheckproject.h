/****************************************************************************
** Copyright (C) 2017 ZhouLiQiu(zhouliqiu@126.com).
**
** This file is part of the qtc_cppcheck, It is a plugin for Qt Creator.
**
** Permission is hereby granted, free of charge, to any person obtaining a copy
** of this software and associated documentation files (the "Software"), to deal
** in the Software without restriction, including without limitation the rights
** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
** copies of the Software, and to permit persons to whom the Software is
** furnished to do so, subject to the following conditions:
**
** The above copyright notice and this permission notice shall be included in
** all copies or substantial portions of the Software.
**
** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
** THE SOFTWARE.
**
****************************************************************************/
#ifndef CDLG_CHECK_PROJECT_H
#define CDLG_CHECK_PROJECT_H

#include <QDialog>
#include <QStringList>
#include <QTreeWidgetItem>
#include <QCloseEvent>
#include <QProcess>

namespace qtc_cppcheck {
namespace Internal {

namespace Ui {
class CDlgCheckProject;
}

class Worker_Check;

class CDlgCheckProject : public QDialog
{
    Q_OBJECT

public:
    explicit CDlgCheckProject(QWidget *parent = 0);
    ~CDlgCheckProject();

protected:
    void closeEvent(QCloseEvent *e);

private slots:
    void on_btnCheck_clicked();
    void on_btnClose_clicked();
    void setCollapsedIcon(QTreeWidgetItem *item);
    void setExpandedIcon(QTreeWidgetItem *item);
    void on_treeWidget_itemClicked(QTreeWidgetItem *item, int column);
    void refreshFile();

    void slot_check_progress(int value);
    void slot_check_finished(int exitCode, QProcess::ExitStatus exitStatus,
                             int error, int warn, int style, int portability, int performance);

private:
    Ui::CDlgCheckProject *ui;
    QTreeWidgetItem *m_headerItem;
    QTreeWidgetItem *m_sourceItem;

    Worker_Check *m_worker_check;

    void initProject();
    int  getFileType(QString fileName);
};

} // namespace Internal
} // namespace qtc_cppcheck

#endif // CDLG_CHECK_PROJECT_H
